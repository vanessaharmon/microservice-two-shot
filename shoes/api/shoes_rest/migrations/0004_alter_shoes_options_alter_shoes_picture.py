# Generated by Django 4.0.3 on 2023-08-30 23:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0003_rename_bin_shoes_bin'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='shoes',
            options={},
        ),
        migrations.AlterField(
            model_name='shoes',
            name='picture',
            field=models.URLField(max_length=500, null=True),
        ),
    ]
