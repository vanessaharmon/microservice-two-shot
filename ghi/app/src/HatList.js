import { Link } from "react-router-dom";
import React, { useState, useEffect } from "react";

function HatList() {
  const [hats, setHats] = useState([]);

  const handleDeleteHat = (id) => {
    if (window.confirm("Are you sure you want to delete?")) {
      fetch(`http://localhost:8090/api/hats/${id}/`, { method: "DELETE" }).then(
        () => {
          window.location.reload();
        }
      );
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8090/api/hats/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <p></p>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link to="/hats/new" className="btn btn-outline-info btn-lg px-4 gap-3">
          Add new hat
        </Link>
      </div>
      <p></p>
      <div className="row row-cols-1 row-cols-md-3 g-4">
        {hats.map((hat) => (
          <div className="col" key={hat.id}>
            <div className="card h-100">
              <img
                src={hat.picture_url}
                className="card-img-top"
                alt={hat.style_name}
              />
              <div className="card-body">
                <h5 className="card-title">{hat.style_name}</h5>
                <p className="card-text">{hat.location}</p>
              </div>
              <div className="card-footer">
                <button className="btn btn-info me-md-2" type="button">
                  Edit
                </button>
                <button
                  onClick={() => {
                    handleDeleteHat(hat.id);
                  }}
                  className="btn btn-info me-md-2"
                  type="button"
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default HatList;
