# Wardrobify
show, create, delete a wardrobe application for a easier and cleaner closet manager
Team:

* Person 1 - Which microservice? Hats Vanessa Harmon
* Person 2 - Which microservice? Shoes Dante Burger

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
Created Shoe model and Bin model. Configured list views and detail views with urls. Implemented a poller to pull Bin data from the Wardrobe API. A list of Shoes is shown on the page with a form to create them, and button to handle delete.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
Created Hat model and Location model. Configured list views and detail views with urls. Implemented a poller to pull Location data from the Wardrobe API. A list of hats is shown on the page with a form to create them, and button to handle delete.
